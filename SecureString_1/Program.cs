﻿//this is me defining my own preprocessor directives

#define HELLO_THERE

//If I am in debug mode, I wish to undefine the HELLO_THERE directive
#if !DEBUG
#undef HELLO_THERE


#endif



using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;



namespace SecureString_1
{



    class Program
    {

        static void Main(string[] args)
        {

            //Trace object for Tracing, look inside the while loop for more details
            TraceSource temp_tracing_object = new TraceSource("tracething1", SourceLevels.All);

            //here, getting all the traced stuff into the OUTPUT section of visual studio
            //be default, the tracing information is 'listened' by the OUTPUT section of visual studio

            //I can change it so that the trace 'flushes' to a text document

            //create a new text file based listener
            Stream file_for_trace_flushing = File.Create("traceherebro.txt");  //this file will be created in the bin of this solution

            //create a listener with the above file
            TextWriterTraceListener text_flush_listener = new TextWriterTraceListener(file_for_trace_flushing);

            //clear existing listeners which at this point would be visual stuiood
            temp_tracing_object.Listeners.Clear();

            //add our text writer as the new listener
            temp_tracing_object.Listeners.Add(text_flush_listener);


            //beginning tracing
            temp_tracing_object.TraceInformation("Have started the tracing activity");

            //also, I will now log an event to the windows event loggers
            log_the_windows_event();

            //now, secure string only works with single chacters
            //hence I use a while loop to collect each key

            //first let me collect input from the user nad store it in the secure string

            using (SecureString temp_secure_string = new SecureString())
            {


                while(true)
                {
                    //I wish to do some logging here
                    //I can use Debug Logs
                    //I can use Trace Logs

                    //here is a Debug Log
                    Debug.WriteLine("Now in the while loop!");  //this line will be printed - during debug - everywhere while loop is 
                                                                //entered. in release method, this method does not get called at all.
                                                                //Trace works in release mode also

                    //the first parameter indicates the type of trace.
                    //second indicates some number that you can choose to mean something
                    //finally the message to display
                    temp_tracing_object.TraceEvent(TraceEventType.Information, 1, "now in while loop");


                    //each key pressed is collected here
                    ConsoleKeyInfo collect_key_from_user = Console.ReadKey();
                    //break out of the while loop if user has enterd Enter
                    if(collect_key_from_user.Key == ConsoleKey.Enter)
                    {
                        temp_tracing_object.TraceEvent(TraceEventType.Critical, 0, "This is a critical event");
                        break;
                    }
                    //if you have reached here - the code I mean - that means user pressed something other than Enter
                    temp_secure_string.AppendChar(collect_key_from_user.KeyChar);


                }


                //this ensures that hte string itself is no longer writeable once outside the while loop
                //that means, nobody can mess with it
                temp_secure_string.MakeReadOnly();

                //okay, time to take a lunch break

                //now I have secure string that cannot be tampered with easily.

                //now trying to get an usable string out of it, and display it
                IntPtr string_from_secure_string = IntPtr.Zero;
                //I will use a try block so that this IntPtr that I am using will be flushed from memory after its job is done
                try
                {
                    string_from_secure_string = Marshal.SecureStringToGlobalAllocUnicode(temp_secure_string);
                    //displaying the obtained string
                    Console.WriteLine("The string is {0}", Marshal.PtrToStringUni(string_from_secure_string));
                }
                finally
                {
                    temp_tracing_object.TraceEvent(TraceEventType.Error, 1, "flushing the memory");
                    //no matter what goes right or wrong in the try block, flushing the IntPtr from all of its contents
                    Marshal.ZeroFreeGlobalAllocUnicode(string_from_secure_string);
                }

            }

            //this is the preprocessor compiler directives
            //the #if directive, for instance, checks if the app is being run in debug mode or non debug mode
            //just remember that if you dont put that #endif at the end, you will run into issues.
#if DEBUG
                Console.WriteLine("Debug version");
            //I wish to raise an error while in debug mode
            //so I will use #error
            //uncomment the following line to get error warning
            //#error We are in debug mode
            //also wish to raise a warning in debug mode. so use #warning
            //uncomment the following line to get error warning
            //#warning Throwing this error, just like that
            //notice how there are no brackets when using # directives.
#else
            Console.WriteLine("not debugging version");
                    //I will also undefine a SYMBOL if this is in release mode


#endif


            //you can use this to disable warnings
#pragma warning disable
            //any code that is between these things will never raise a warning
#pragma warning restore
            //becuse of the conditional usage, this method never works in debug mode
            trythis();


            //now lets flush to the listener. 
            temp_tracing_object.Flush();
            //now closing it to save memory
            temp_tracing_object.Close();
            //as always, NEVER let the console from dissapearing.
            Console.ReadLine();
        }

        //logging a simple event to the windows event logging system
        private static void log_the_windows_event()
        {
            //first check if a particular eventlog source exists
            if(EventLog.SourceExists("temp_event_source") == false)
            {
                //temp_event_log event item does not exist. so lets create one
                //this item will be like a file or a book into which our app will write to
                EventLog.CreateEventSource("temp_event_source", "temp_event_log");
                Console.WriteLine("in order to write into the log, rerun the app after quitting it");
                return;
            }

            EventLog log_temp_for_event = new EventLog();
            //when this app was run for the first time, a source with the name temp_event_source would have been created
            //thanks to the if block above
            log_temp_for_event.Source = "temp_event_source";
            string time_temp = DateTime.Now.ToString();
            //writing a simple entry
            log_temp_for_event.WriteEntry("Hello there!!! this is from the secure string app " + time_temp);
            //open the event viewer on your computer to see this in action
        }

        //I can also use these to disable some methods from being called
        [Conditional("DEBUG")]
        //along with these you can use DebuggerDisplay to display object values
        public static void trythis()
        {
            Console.WriteLine("Ha! we are in release mode!");
        }
    }
}


